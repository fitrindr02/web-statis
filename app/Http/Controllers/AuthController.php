<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('form');
    }
    
    public function Send(Request $request){
        $nama_depan = $request->nama_depan;
        $nama_belakang = $request->nama_belakang;

        return view("/welcome", compact('nama_depan', 'nama_belakang'));
    }
}
