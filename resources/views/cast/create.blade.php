@extends('layout.master')
@section('judul')
	TAMBAH DATA CAST
@endsection
@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Tambah Data</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="/cast">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label>Nama</label>
          <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama">
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
          <label>Umur</label>
          <input type="number" name="umur" class="form-control" placeholder="Umur">
          @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
          @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" id="bio" class="form-control" rows="3"></textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Tambah</button>
      </div>
    </form>
  </div>

@endsection