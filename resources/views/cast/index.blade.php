@extends('layout.master')
@section('judul')
	DATA CAST
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary">Tambah</a>
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th>NO</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr> 
        @endforelse
      
    </tbody>
  </table>
@endsection