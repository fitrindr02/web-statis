<!DOCTYPE html>
@extends('layout.master')
@section('judul')
	Halaman Form
@endsection
@section('content')
	
	<form action="/send" method="POST">
		@csrf
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>
			<p>First Name:</p>
			<input type="text" name="nama_depan">
			<p>Last Name:</p>
			<input type="text" name="nama_belakang">
			<p>Gender:</p>
			<input type="radio" name="Male">Male
			<br>
			<input type="radio" name="Female">Female
			<br>
			<input type="radio" name="Other">Other
			<br>
			<p>Nationality:</p>
			<select name="negara">
				<option value="Indonesian">Indonesian</option>
				<option value="Singaporean">Singaporean</option>
				<option value="Malaysian">Malaysian</option>
				<option value="Australian">Australian</option>
			</select>
			<p>Language Spoken:</p>
			<input type="checkbox" name="b_indonesia">Bahasa Indonesia
			<br>
			<input type="checkbox" name="English">English
			<br>
			<input type="checkbox" name="Other">Other
			<br>
			<p>Bio:</p>
			<textarea name="bio" rows="10" cols="30"></textarea>
			<br>
		<button>Sign Up</button>
	</form>
@endsection
